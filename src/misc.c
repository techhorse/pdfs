/* misc.c
 * Copyright (C) 2023  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <misc.h>
#include <stdint.h>
#include <time.h>


int64_t time_ns64()
{
  struct timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);
  return ((int64_t)ts.tv_sec*1000000000) + ts.tv_nsec;
}

int64_t ts_ns64(struct timespec ts)
{
  return ((int64_t)ts.tv_sec*1000000000) + ts.tv_nsec;
}

size_t hexstr_to_u8arr(const char* str, uint8_t *data, size_t outsize)
{
  size_t n = 0; //number of hex char
  uint8_t tmp = 0;
  while(n<outsize*2){
    char ch = *str;
    if('0'<=ch && ch<='9'){
      tmp |= ch-'0';
    }else if('a'<=ch && ch<='f'){
      tmp |= ch-'a'+10;
    }else if('A'<=ch && ch<='F'){
      tmp |= ch-'A'+10;
    }else if(ch == ' '){
      str++;
      continue;
    }else{
      break;
    }
    str++;
    n++;
    if(n%2 == 0){
      *data++ = tmp;
      tmp = 0;
    }else{
      tmp <<= 4;
    }
  }
  return n/2;
}
