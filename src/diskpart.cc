/* diskpart.cc: Get disks and parts info
 * Copyright (C) 2023  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "diskpart.hh"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <linux/hdreg.h>
#include <blkid/blkid.h>
#include <dirent.h>

#include <map>
#include <string>
#include <iostream>

#include "db.hh"
#include "log.h"

#define DEVDIR     "/sys/block"
#define MOUNTSLIST "/proc/mounts"  // 或"/etc/mtab"
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))

using namespace std;

map<string, ndid_t> dp2nd;  //convert devpath to ndid_t
PartsTab ptab;

int PartsTab::add(partinfo &p)
{
  vector<partinfo>::iterator it = vec.begin();
  for(;it!=vec.end();it++){
    if((p.npid.nid!=0 && it->npid==p.npid) or \
       (p.uuid.length() && it->uuid==p.uuid) or \
       (p.devpath.length() && it->devpath==p.devpath) or \
       (p.moupath.length() && it->moupath==p.moupath)){
      return -1;
    }
  }
  vec.push_back(p);
  return 0;
}

partinfo *PartsTab::get_bynpid(const npid_t &npid)
{
  vector<partinfo>::iterator it = vec.begin();
  for(;it!=vec.end();it++){
    if(it->npid==npid){
      return &*it;
    }
  }
  return nullptr;
}

partinfo *PartsTab::get_byuuid(const string &uuid)
{
  vector<partinfo>::iterator it = vec.begin();
  for(;it!=vec.end();it++){
    if(it->uuid==uuid){
      return &*it;
    }
  }
  return nullptr;
}

partinfo *PartsTab::get_bydevpath(const string &devpath)
{
  vector<partinfo>::iterator it = vec.begin();
  for(;it!=vec.end();it++){
    if(it->devpath==devpath){
      return &*it;
    }
  }
  return nullptr;
}

partinfo *PartsTab::get_bymoupath(const string &moupath)
{
  vector<partinfo>::iterator it = vec.begin();
  for(;it!=vec.end();it++){
    if(it->moupath==moupath){
      return &*it;
    }
  }
  return nullptr;
}

void PartsTab::print()
{
  vector<partinfo>::iterator it = vec.begin();
  for(;it!=vec.end();it++){
    if(it->npid.nid){
      cout << "nid=" << it->npid.nid << ",pid=" << it->npid.pid << endl;
    }
    if(it->uuid.length()){
      cout << "uuid=" << it->uuid << endl;
    }
    if(it->devpath.length()){
      cout << "devpath=" << it->devpath << endl;
    }
    if(it->moupath.length()){
      cout << "moupath=" << it->moupath << endl;
    }
    cout << endl;
  }
}

static int strncpy_noendspace(char *dst, const char *src, size_t n)
{
  if(dst == NULL){
    return -1;
  }
  int m = n;
  while(m>0 && src[--m]==' ');
  for(int i=0;i<=m;i++){
    dst[i] = src[i];
  }
  for(int i=m+1;i<=n;i++){
    dst[i] = '\0';
  }
  return m;
}

//目前无法获取U盘序列号
int disk_getinfo(const char *devpath, char *model, char *serno)
{
  struct hd_driveid hid;
  int fd = open(devpath, O_RDONLY | O_NONBLOCK);
  if(fd < 0){
    perror("open");
    return 1;
  }
  if(ioctl(fd, HDIO_GET_IDENTITY, &hid)){
    perror("ioctl");
    close(fd);
    return 1;
  }
  close(fd);
  if(model){
    strncpy_noendspace(model, (char *)hid.model, ARRAY_SIZE(hid.model));
  }
  if(serno){
    strncpy_noendspace(serno, (char *)hid.serial_no, ARRAY_SIZE(hid.serial_no));
  }
  return 0;
}

static char path_up(char *path, int *n)
{
  while(*n>1 && path[--*n]!='/');
  char ch = path[*n];
  path[*n] = '\0';
  return ch;
}

static int path_append(char *dst, const char *src, size_t dstsize)
{
  int m;
  if(src[0] == '/'){
    m = stpncpy(dst, src, dstsize) - dst;
  }else{
    m = strnlen(dst, dstsize);
    if(m >= dstsize){
      return -1;
    }
    if(m-1>=0 && dst[m-1]!='/'){
      dst[m++] = '/';
    }
    m = stpncpy(&dst[m], src, dstsize-m) - dst;
  }
  return m;
}

static int stat_nolink(char *s2, struct stat *info)
{
  if(lstat(s2, info) != 0){
    perror("lstat");
    return -1;
  }
  if(S_ISLNK(info->st_mode)){
    return -1;
  }
  return 0;
}

//将路径转化为无符号链接的绝对路径
int path_abs_nolink(const char *path, char *buf, size_t bufsize)
{
  int n1, n2, i1, i2, ret;
  struct stat info;
  char *concate = (char *)malloc(4096);
  if(concate == NULL){
    return -1;
  }
  char *tmppath = (char *)malloc(4096);
  if(tmppath == NULL){
    return -1;
  }
  char *linkbuf = (char *)malloc(4096);
  if(linkbuf == NULL){
    return -1;
  }
  if(path[0] != '/'){
    //convert to abs path
    if(!getcwd(concate, 4096)){
      perror("getcwd");
      ret = -1;
      goto end;
    }
    if(concate[0] != '/'){
      fprintf(stderr, "getcwd result not start with '/'");
      ret = -1;
      goto end;
    }
    n1 = path_append(concate, path, 4096);
    if(n1 >= 4096){
      ret = -1;
      goto end;
    }
  }else{
    strcpy(concate, path);
  }
  tmppath[0] = '\0';
  n2 = 0;
  i2 = 0;
  do{
    i1 = i2;
    do{
      i2++;
    }while(concate[i2]!='/' && concate[i2]!='\0');
    if(i2==i1+2 && concate[i1+1]=='.'){
      continue;
    }
    if(i2==i1+3 && concate[i1+1]=='.' && concate[i1+2]=='.'){
      path_up(tmppath, &n2);
      continue;
    }
    for(;i1<i2;i1++){
      tmppath[n2++] = concate[i1];
    }
    tmppath[n2] = '\0';
    if(lstat(tmppath, &info) != 0){
      ret = -1;
      goto end;
    }
    if(S_ISLNK(info.st_mode)){
      readlink(tmppath, linkbuf, 4096);
      path_up(tmppath, &n2);
      n2 = path_append(tmppath, linkbuf, 4096);
    }
  }while(concate[i2]!='\0');
  strncpy(buf, tmppath, bufsize);
  ret = n2;
end:
  free(concate);
  free(tmppath);
  free(linkbuf);
  return ret;
}

//获取路径的挂载点分割，不支持相对路径和符号链接，请先用path_abs_nolink转换
int path2mpcut(const char *path)
{
  dev_t dev;
  int n, m;
  char *s2 = (char *)malloc(4096);
  if(s2 == NULL){
    return -1;
  }
  strncpy(s2, path, 4096);
  n = strnlen(s2, 4096);
  if(n >= 4096){
    return -1;
  }
  struct stat info;
  if(stat_nolink(s2, &info) != 0){
    n = -1;
    goto end;
  }
  dev = info.st_dev;
  m = n;
  while(n > 1){
    path_up(s2, &n);
    if(stat_nolink(s2, &info) != 0){
      n = -1;
      goto end;
    }
    if(info.st_dev != dev){
      break;
    }else{
      m = n;
    }
  }
end:
  free(s2);
  return m;
}

int part_getuuid(const char *path, char *buf, size_t bufsize)
{
  int ret = 0;
  const char *tmp;
  blkid_probe pr;
  pr = blkid_new_probe_from_filename(path);
  if(!pr){
    perror("blkid_new_probe_from_filename");
    return -1;
  }
  blkid_do_probe(pr);
  blkid_probe_lookup_value(pr, "UUID", &tmp, NULL);
  if(tmp){
    strncpy(buf, tmp, bufsize);
  }else{
    ret = -1;
  }
  blkid_free_probe(pr);
  return ret;
}

int dp_newpart(ndid_t disk, partinfo *p)
{
  npid_t part = db_newpart(disk, p->uuid.c_str());
  if(part.nid == 0){
    return -1;
  }
  p->npid = part;
  return 0;
}

int disks_init()
{
  int n = 0;
  DIR *dir;
  char s[32] = "/dev/";
  char model[41], serno[21];
  ndid_t ndid;
  struct dirent *ditem;
  LOGI("start find disks\n");
  dir = opendir(DEVDIR);
  if(dir == NULL){
    return -1;
  }
  while((ditem=readdir(dir)) != NULL){
    if(ditem->d_name[0]=='s' && ditem->d_name[1]=='d'){
      strncpy(&s[5], ditem->d_name, sizeof(s)-5-1);
      if(disk_getinfo(s, model, serno) == 0){
	LOGI("model=%s, serno=%s\n", model, serno);
	ndid = db_finddisk(model, serno);
	if(ndid.nid){
	  dp2nd[s] = ndid;
	}
	n++;
      }
    }
  }
  closedir(dir);
  LOGI("finish find disks\n");
  return n;
}

int parts_init()
{
  int n = 0;
  FILE *fp;
  fp = fopen(MOUNTSLIST, "r");
  if(!fp){
    return -1;
  }
  char devpath[256], moupath[256], format[256], uuid[256];
  npid_t npid;
  partinfo pinfo;
  LOGI("start find parts\n");
  while(!feof(fp)){
    //设备路径 挂载点 格式 参数 dump pass
    fscanf(fp, "%s %s %s %*d %*d%*c", devpath, moupath, format);
    if(strncmp(devpath, "/dev/sd", 7) == 0){
      if(part_getuuid(devpath, uuid, 256) == 0){
	LOGI("devpath=%s, moupath=%s, uuid=%s\n", devpath, moupath, uuid);
        npid = db_findpart(uuid);
      }else{
	uuid[0]='\0';
	npid = {0,0};
      }
      pinfo.uuid = uuid;
      pinfo.npid = npid;
      pinfo.devpath = devpath;
      pinfo.moupath = moupath;
      ptab.add(pinfo);
      n++;
    }
  }
  LOGI("finish find parts\n");
  return n;
}

int dp_init()
{
  int st=0;
  if(disks_init()!=0){
    st=1;
  }
  if(parts_init()!=0){
    st=1;
  }
  return st;
}
