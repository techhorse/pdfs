/* cli.c: Command Line Interface
 * Copyright (C) 2023-2024  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <bsd/stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <getopt.h>

#include "misc.h"
#include "readconf.h"
#include "diskpart.hh"
#include "log.h"
#include "hash.h"
#include "msgproc.hh"

#include <iostream>

using namespace std;


#define streq(a, b) (strcmp((a), (b)) == 0)

const char help_docs[] =
  "PDFS CLI\n"
  "pdfs [--help] <command> [<args>]";

int ki_qid, ko_qid;

static bool enter_y_or_n()
{
  while(1){
    char c;
    cin >> c;
    if(c == 'y' || c == 'Y'){
      return true;
    }else if(c == 'n' || c == 'N'){
      return false;
    }else{
      cout << "请输入字符'y'或'n':";
    }
  }
}

static struct option long_options_add[] = {
  {"fixed",          no_argument,       0, 'f' }, //固定
  {"sync",           no_argument,       0, 's' }, //同步
  {"recursive",      no_argument,       0, 'r' }, //递归
  {"entry-compress", no_argument,       0, 'c' }, //进入压缩包
  {"follow",         no_argument,       0, 'L' }, //跟随符号链接
  {"bigfile-size",   required_argument, 0, 'B' }, //不要读取大文件
};

int cmd_add(int argc, char *argv[])
{
  struct msgcmd msg;
  Para_cmd_addfile &para = *(Para_cmd_addfile *)&msg.mtext;
  char *path = (char *)malloc(4096);  //临时保存路径
  if(path == NULL){
    return 1;
  }
  char *p2;
  memset(&para, 0, sizeof(para));
  while(1){
    int option_index, c;
    c = getopt_long(argc, argv, "fsrcLB:", long_options_add, &option_index);
    if(c == -1){
      break;
    }
    switch(c){
    case 'f':
      para.fixed = true;
      break;
    case 's':
      para.sync = true;
      break;
    case 'r':
      para.recursive = true;
      break;
    case 'c':
      para.entry_compress = true;
      break;
    case 'L':
      para.follow = true;
      break;
    case 'B':
      if(dehumanize_number(optarg, &para.bigfile_thresold)){
	perror("Bigfile thresold");
      }
      break;
    default:
      break;
    }
  }
  if(optind != argc-1){
    printf("need path\n");
    return 1;
  }
  //转换成无符号链接的绝对路径
  path_abs_nolink(argv[optind], path, 4096);
  LOGI("add %s\n", path);
  msg.mtype = MsgCMD_AddFile;
  msg.pid = getpid();
  strncpy(msg.mtext+sizeof(Para_cmd_addfile), path, 1000);
  msgsnd(ki_qid, &msg, sizeof(pid_t)+sizeof(Para_cmd_addfile)+strlen(path)+1, 0);
  free(path);
}

int cmd_hash(int argc, char *argv[])
{
  if(argc != 2){
    return 1;
  }
  struct stat sb;
  stat(argv[1], &sb);
  if(!S_ISREG(sb.st_mode)){
    printf("'%s' is not regular file\n", argv[1]);
    return 1;
  }

  uint8_t *hash = (uint8_t *)malloc(calc_hashs_length(FLAG_HASHALL));
  printf("hashs of file '%s':\n", argv[1]);
  int ret = calc_file_hashs(argv[1], FLAG_HASHALL, hash);
  if(ret != 0){
    printf("calc faild\n");
    return 1;
  }
  printf("MD5: ");
  for(int i=0;i<16;i++){
    printf("%02x", hash[i]);
  }
  printf("\nSHA1: ");
    for(int i=0;i<20;i++){
    printf("%02x", hash[16+i]);
  }
  printf("\nSHA256: ");
    for(int i=0;i<32;i++){
    printf("%02x", hash[16+20+i]);
  }
  printf("\nSHA512: ");
  for(int i=0;i<64;i++){
    printf("%02x", hash[16+20+32+i]);
  }
  printf("\n");
  free(hash);
  return 0;
}

int cmd_findprop(int argc, char* argv[])
{
  uint8_t buff[256];
  struct msgcmd msg;
  if(argc != 3){
    printf("must two param\n");
    return 1;
  }
  msg.mtype = MsgCMD_FindProp;
  msg.pid = getpid();
  Para_cmd_findprop &para = *(Para_cmd_findprop *)&msg.mtext;
  para.len_name = strlen(argv[1])+1;
  para.len_data = hexstr_to_u8arr(argv[2], buff, 255);
  strcpy(msg.mtext+sizeof(Para_cmd_findprop), argv[1]);
  memcpy(msg.mtext+sizeof(Para_cmd_findprop)+para.len_name, buff, para.len_data);
  msgsnd(ki_qid, &msg, sizeof(pid_t)+sizeof(Para_cmd_findprop)+para.len_name+para.len_data, 0);
  msgrcv(ko_qid, &msg, sizeof(msg)-sizeof(long), 0, 0);
  nfid_t &resid = *(nfid_t *)&msg.mtext;
  printf("findprop: nid=%ld,fid=%ld\n", resid.nid, resid.fid);
}

int cmd_help(int argc, char *argv[])
{
  puts(help_docs);
  return 0;
}

struct cmd{
  const char *name;
  int (*func)(int, char **);
}const cmds[] = {
  {"add", cmd_add},
  {"hash", cmd_hash},
  {"findprop", cmd_findprop},
  {"help", cmd_help},
  {NULL}
};

int main(int argc, char *argv[])
{
  int (*func)(int, char**) = cmd_help;
  int ret;
  if(argc < 2){
    return cmd_help(0, NULL);
  }
  const struct cmd *p = cmds;
  while(p->name){
    if(streq(p->name, argv[1])){
      func = p->func;
      break;
    }
    p++;
  }
  //TODO: 改为socket, 避免与多个程序同时通信产生混乱
  ki_qid = msgget(ftok(PATH_IPC, 'i'), IPC_CREAT | 0660);
  ko_qid = msgget(ftok(PATH_IPC, 'o'), IPC_CREAT | 0660);
  ret = func(argc-1, argv+1);
  return ret;
}
