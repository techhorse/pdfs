/* msgproc.cc: process of message queue
 * Copyright (C) 2023-2024  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <map>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#include "msgproc.hh"
#include "diskpart.hh"
#include "misc.h"
#include "log.h"
#include "hash.h"

using namespace std;

bool auto_newdisk = 0;
bool auto_newpart = 0;

extern map<string, ndid_t> dp2nd;
extern map<npid_t, nfid_t> p2f;
extern PartsTab ptab;

/*
 * 将目录树记录到数据库
 * para base: 基本nfid_t
 * para target: 已有相同文件的nfid, nid=0表示没有
 * para path: 总路径
 * para cut: 路径切割,path[cut]
 *
 * base与path[:cut]视为同一目录，逐级记录path
 * 运行过程中会改变path，但最终不变
 */
nfid_t db_recdirs(nfid_t base, nfid_t target, char *path, int cut, bool fixed, bool sync)
{
  char name[256];
  char tmp;
  int n; //名称的字符串长度（不含'\0'）
  int i1, i2=cut;
  nfid_t nf1, nf2=base;
  bool scan=true;  //是否继续扫描数据库
  struct stat info;
  dbmode_t dbmode;
  while(path[i2] != '\0'){
    while(path[i2] == '/'){
      i2++; //跳过当前的'/'
    }
    i1 = i2; //记为字符串开始
    nf1 = nf2; //将上次操作的文件夹对象作为父文件夹
    while(path[i2]!='/' && path[i2]!='\0'){
      i2++;
    }
    n = i2-i1; //当前级文件(夹)名长度
    if(n == 0){
      continue;
    }
    if(n >= 255){
      goto err;
    }
    strncpy(name, path+i1, n); //截取字符串name=path[i1:i2]
    name[n] = '\0';
    if(scan){
      nf2 = db_getnamedchild(nf1, name);
      if(nf2.nid == 0){ //在数据库里没有找到该级目录
	scan = false; //下次不要去数据库找了
      }
    }
    if(!scan){
      tmp = path[i2];
      path[i2] = '\0'; //临时切断path到path[:i2]
      lstat(path, &info);
      if(S_ISDIR(info.st_mode)){
        dbmode = DB_DIR;
      }else if(S_ISREG(info.st_mode)){
        dbmode = DB_FILE;
      }else{
        goto err;
      }
      if(tmp == 0 && target.nid != 0){
	//当前是最后一级文件(夹) 且 目标文件的nfid是`target`
	nf2 = target;
      }else{
	nf2 = db_newfile(name, 0, dbmode, fixed, sync,
			 ts_ns64(info.st_atim), ts_ns64(info.st_mtim), ts_ns64(info.st_ctim));
      }
      path[i2] = tmp;
      if(db_newrelat(nf1, nf2) != 0){
        goto err;
      }
    }
    if(nf2.nid == 0){
      goto err;
    }
  }
  return nf2;
err:
  return {0, 0};
}

ndid_t adddisk(string diskpath)
{
  ndid_t ndid;
  char model[41], serno[21];
  if(disk_getinfo(diskpath.c_str(), model, serno) != 0){
    return {0, 0};
  }
  ndid = db_newdisk(model, serno);
  if(ndid.nid == 0){
    return {0, 0};
  }
  dp2nd[diskpath] = ndid;
  return ndid;
}

MsgRsp addpart(partinfo *p)
{
  string rmnum = p->devpath; //去掉后面的分区编号数字
  if('0' <= *(rmnum.end()-1) <= '9'){
    rmnum.pop_back();
  }
  ndid_t ndid;
  if(dp2nd.count(rmnum) == 0){
    //数据库中没有磁盘记录
    if(auto_newdisk == false){
      LOGE("Disk not record on db, and don't auto create\n");
      return MsgRsp_DiskNF;
    }else{
      ndid = adddisk(rmnum);
    }
  }else{
    ndid = dp2nd[rmnum];
  }
  if(ndid.nid == 0){
    return MsgRsp_DBErr;
  }
  dp_newpart(ndid, p);
  return MsgRsp_OK;
}

//TODO: 改用循环,跟随符号链接时检测环
MsgRsp addfile(const Para_cmd_addfile &para, nfid_t base, char *path, int mpcut)
{
  struct stat sb;
  nfid_t resid = {0, 0};
  uint8_t *hash = NULL;
  int hret = -1;
  stat(path, &sb);
  if(S_ISREG(sb.st_mode)){
    hash = (uint8_t *)malloc(calc_hashs_length(FLAG_SHA256));
    hret = calc_file_hashs(path, FLAG_SHA256, hash);
    if(hret != 0){
      LOGI("Calc hash faild\n");
    }
  }
  if(hret == 0){
    resid = db_findprop("SHA256", hash, 32);
  }
  bool hash_found = (resid.nid!=0);
  LOGI("add %s, ", path);
  resid = db_recdirs(base, resid, path, mpcut, para.fixed, para.sync); //文件nfid
  if(!hash_found){
    if(hash != NULL){
      db_newprop(resid, "SHA256", hash, 32);
    }
    //TODO: 待修复：相同文件名存在时，没有创建新文件也会显示`created`
    LOGI("created nid=%ld, fid=%ld\n", resid.nid, resid.fid);
  }else if(resid.nid != 0){
    LOGI("matched nid=%ld, fid=%ld\n", resid.nid, resid.fid);
  }else{
    LOGI("not get nfid\n");
  }
  if(para.recursive && S_ISDIR(sb.st_mode)){
    DIR *dir;
    struct dirent *ent;
    char *path2 = (char *)malloc(strlen(path)+257);
    if(path2 == NULL){
      return MsgRsp_OtherErr;
    }
    dir = opendir(path);
    if(dir == NULL){
      LOGE("open faild\n");
    }
    while((ent = readdir(dir)) != NULL){
      if(strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0){
	sprintf(path2, "%s/%s", path, ent->d_name);
	addfile(para, resid, path2, strlen(path));
      }
    }
    closedir(dir);
    free(path2);
  }
  if(hash != NULL){
    free(hash);
  }
  return MsgRsp_OK;
}

MsgRsp msgcmd_addfile(const void *pmsg, size_t length, void *pout, size_t &osize)
{
  char buf[4096];
  char tmp;
  int mpcut;
  partinfo *p;
  npid_t part;
  nfid_t pfile, file;
  osize = 0;
  const Para_cmd_addfile &para = *(const Para_cmd_addfile *)pmsg;
  const char *path = (const char *)pmsg+sizeof(Para_cmd_addfile);
  LOGI("add file path:%s, fixed:%d, sync:%d\n",
       path, para.fixed, para.sync);
  strncpy(buf, path, sizeof(buf)-1);
  //获取被添加文件所在挂载点
  if((mpcut=path2mpcut(buf)) <= 0){
    return MsgRsp_PartNF;
  }
  //判断挂载点分区是否已添加，已添加则转npid_t，否则提示添加
  if(mpcut > 1){
    tmp = buf[mpcut];
    buf[mpcut] = '\0';
    LOGI("mount path of the file %s\n", buf);
    p = ptab.get_bymoupath(buf);
    buf[mpcut] = tmp;
  }else{
    p = ptab.get_bymoupath("/");
  }
  if(p == nullptr){
    LOGE("Part not found\n");
    return MsgRsp_PartNF;
  }
  if(p->npid.nid == 0){
    //数据库中没有分区记录
    if(auto_newpart == false){
      LOGE("Part not record on db, and don't auto create\n");
      return MsgRsp_PartNF;
    }else{
      addpart(p);
    }
  }
  part = p->npid;
  if(p->npid.nid == 0){
    LOGE("get nid of part error");
    return MsgRsp_DBErr;
  }
  //在数据库中找到所在目录的nfid
  pfile = p2f[part]; //分区nfid
  if(pfile.nid == 0){
    LOGE("get fid of part error");
    return MsgRsp_OtherErr;
  }
  addfile(para, pfile, buf, mpcut);
  //插入数据库文件列表(files表)，并与所在目录添加关系(relat表)
  //新文件要储存在哪些分区(db_getparts函数)，要向哪些节点发送数据
  LOGI("add file OK\n");
  return MsgRsp_OK;
}

MsgRsp msgcmd_findprop(const void *pmsg, size_t length, void *pout, size_t &osize)
{
  nfid_t resid = {0, 0};
  if(length < sizeof(Para_cmd_findprop)){
    LOGE("cmd_findprop length not enough\n");
    return MsgRsp_ParaErr;
  }
  const Para_cmd_findprop &para = *(const Para_cmd_findprop *)pmsg;
  const char *name = (const char *)pmsg+sizeof(Para_cmd_findprop);
  const uint8_t *data = (const uint8_t *)pmsg+sizeof(Para_cmd_findprop)+para.len_name;
  if(sizeof(Para_cmd_findprop)+sizeof(pid_t)+para.len_name+para.len_data != length){
    LOGE("message length mistake\n");
    return MsgRsp_ParaErr;
  }
  if(strlen(name)+1 != para.len_name){
    LOGE("name length mistake, %ld,%d\n", strlen(name), para.len_name);
    return MsgRsp_ParaErr;
  }
  resid = db_findprop(name, data, para.len_data);
  printf("findprop: nid=%ld,fid=%ld\n", resid.nid, resid.fid);
  osize = sizeof(resid);
  memcpy(pout, &resid, sizeof(resid));
  return MsgRsp_OK;
}
