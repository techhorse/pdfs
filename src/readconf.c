/* readconf.c: read config file
 * Copyright (C) 2023  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "readconf.h"
#include "log.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define streq(a, b)  (strcmp((a), (b)) == 0)
#define strin(a, b)  (strncmp((a), (b), strlen(b)) == 0)

extern uint64_t local_nid;
extern bool auto_newdisk;
extern bool auto_newpart;

void parser_conf(const char *text)
{
  while(*text){
    if(strin(text, "local_nid")){
      while(*text++ != '=');
      local_nid = atoi(text);
    }
    if(strin(text, "auto_newdisk")){
      while(*text++ != '=');
      while(*text == ' '){
	text++;
      }
      auto_newdisk = strin(text, "true")?true:false;
    }
    if(strin(text, "auto_newpart")){
      while(*text++ != '=');
      while(*text == ' '){
	text++;
      }
      auto_newpart = strin(text, "true")?true:false;
    }
    while(*text++ != '\n');
  }
}

void load_conf(const char *fn)
{
  FILE *fp = fopen(fn, "r");
  if(fp == NULL){
    LOGE_ERRNO("open conf file");
    return;
  }
  fseek(fp, 0, SEEK_END);
  int file_size = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  char *buff = malloc(file_size);
  fread(buff, 1, file_size, fp);
  fclose(fp);
  parser_conf(buff);
  free(buff);
}
