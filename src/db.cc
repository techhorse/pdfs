/* db.c: Packed database API
 * Copyright (C) 2023-2024  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "db.hh"
#include "diskpart.hh"
#include "misc.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <deque>
#include <map>
#include <set>

#include "sqlite3.h"

using namespace std;

sqlite3 *db;
int64_t local_nid;
int64_t local_last_did;
int64_t local_last_pid;
int64_t local_last_fid;

sqlite3_stmt *stmt_incfid;
sqlite3_stmt *stmt_incpid;
sqlite3_stmt *stmt_incdid;
sqlite3_stmt *stmt_newfile;
sqlite3_stmt *stmt_newrelat;
sqlite3_stmt *stmt_getparent;
sqlite3_stmt *stmt_getchildren;
sqlite3_stmt *stmt_getnamedchild;
sqlite3_stmt *stmt_newpart;
sqlite3_stmt *stmt_findpart;
sqlite3_stmt *stmt_newdisk;
sqlite3_stmt *stmt_finddisk;
sqlite3_stmt *stmt_newprop;
sqlite3_stmt *stmt_findprop;

extern PartsTab ptab;

//暂时只支持一个目录最多对应一个分区
map<nfid_t, npid_t> f2p;
map<npid_t, nfid_t> p2f;
map<npid_t, ndid_t> p2d;

int db_init(const char *fn)
{
  char *s;
  sqlite3_open(fn, &db);
  sqlite3_exec(db, "PRAGMA foreign_keys = ON;", NULL, NULL, NULL);
  sqlite3_exec(db,
	       "CREATE TABLE IF NOT EXISTS nodes("	//节点表
	       "nid INTEGER PRIMARY KEY,"		//节点ID
	       "name NVARCHAR(255),"			//名称
	       "pubkey BLOB,"				//ed25519公钥
	       "last_did INTEGER,"                      //最后磁盘编号
	       "last_pid INTEGER,"                      //最后分区编号
	       "last_fid INTEGER,"			//最后文件编号
	       "last_conn INTEGER,"			//最近连接时间
	       "addrs TEXT);",				//通信地址
	       NULL, NULL, NULL);
  sqlite3_exec(db,
	       "CREATE TABLE IF NOT EXISTS disks("	//磁盘表
	       "nid INTEGER,"				//最早发布的节点ID
	       "did INTEGER,"				//该节点发布的磁盘编号
	       "name NVARCHAR(255),"			//名称
	       "cap INTEGER,"				//容量
	       "rnid INTEGER,"				//常驻节点ID(可选)
	       "model NVARCHAR(255),"			//型号
	       "serno NVARCHAR(255),"			//序列号
	       "PRIMARY KEY(nid, did),"
	       "FOREIGN KEY(nid) REFERENCES nodes(nid),"
	       "FOREIGN KEY(rnid) REFERENCES nodes(nid));",
	       NULL, NULL, NULL);
  sqlite3_exec(db,
	       "CREATE TABLE IF NOT EXISTS files("	//文件表
	       "nid INTEGER,"				//最早发布的节点ID
	       "fid INTEGER,"				//该节点发布的文件编号
	       "name NVARCHAR(255),"			//文件名
	       "len INTEGER,"				//长度
	       "mode INTEGER,"                          //模式
	       "fixed BOOL,"				//是否固定
	       "sync BOOL,"				//是否同步
	       "atime INTEGER,"				//访问世界
	       "mtime INTEGER,"				//修改时间
	       "ctime INTEGER,"				//改变时间
	       "ptime INTEGER,"				//首次发布时间
	       "lcnid INTEGER,"				//最后改变nid
	       "log TEXT,"				//日志
	       "PRIMARY KEY(nid, fid),"
	       "FOREIGN KEY(nid) REFERENCES nodes(nid));",
	       NULL, NULL, NULL);
  sqlite3_exec(db,
	       "CREATE TABLE IF NOT EXISTS parts("	//分区表
	       "nid INTEGER,"				//最早发布的节点ID
	       "pid INTEGER,"				//该节点发布的分区编号
	       "name NVARCHAR(255),"			//名称
	       "len INTEGER,"				//长度
	       "fmt NVARCHAR(255),"			//格式
	       "uuid VARCHAR(255),"                     //UUID
	       "dnid INTEGER,"				//所在磁盘nid
	       "ddid INTEGER,"				//所在磁盘did
	       "fnid INTEGER,"				//根目录nid
	       "ffid INTEGER,"				//根目录fid
	       "PRIMARY KEY(nid, pid)"
	       "FOREIGN KEY(nid) REFERENCES nodes(nid),"
	       "FOREIGN KEY(dnid, ddid) REFERENCES disks(nid, did),"
	       "FOREIGN KEY(fnid, ffid) REFERENCES files(nid, fid));",
	       NULL, NULL, NULL);
  sqlite3_exec(db,
	       "CREATE TABLE IF NOT EXISTS relat("      //邻接表
	       "id INTEGER PRIMARY KEY AUTOINCREMENT,"
	       "nid1 INTEGER,"
	       "fid1 INTEGER,"
	       "nid2 INTEGER,"
	       "fid2 INTEGER,"
	       "FOREIGN KEY(nid1,fid1) REFERENCES files(nid,fid),"
	       "FOREIGN KEY(nid2,fid2) REFERENCES files(nid,fid));",
	       NULL, NULL, NULL);
  sqlite3_exec(db,
	       "CREATE TABLE IF NOT EXISTS props("
	       "nid INTEGER,"
	       "fid INTEGER,"
	       "key NVARCHAR(255),"
	       "value BLOB,"
	       "FOREIGN KEY(nid,fid) REFERENCES files(nid,fid));",
	       NULL, NULL, NULL);
  s=sqlite3_mprintf("INSERT INTO nodes (nid,last_did,last_pid,last_fid) VALUES (%ld,0,0,0);", local_nid);
  sqlite3_exec(db, s, NULL, NULL, NULL);
  sqlite3_free(s);

  //更新fid,pid,did
  sqlite3_prepare_v2(db,
		     "UPDATE nodes SET last_fid=? WHERE nid=?",
		     -1, &stmt_incfid, NULL);
  sqlite3_prepare_v2(db,
		     "UPDATE nodes SET last_pid=? WHERE nid=?",
		     -1, &stmt_incpid, NULL);
  sqlite3_prepare_v2(db,
		     "UPDATE nodes SET last_did=? WHERE nid=?",
		     -1, &stmt_incdid, NULL);

  //文件
  s=sqlite3_mprintf("INSERT INTO files(nid,fid,name,len,mode,fixed,sync,atime,mtime,ctime,ptime) "
		    "VALUES(%ld,?,?,?,?,?,?,?,?,?,?)",
		    local_nid);
  sqlite3_prepare_v2(db, s, -1, &stmt_newfile, NULL);
  sqlite3_free(s);

  //关系记录
  sqlite3_prepare_v2(db,
		     "INSERT INTO relat(nid1,fid1,nid2,fid2) VALUES(?,?,?,?)",
		     -1, &stmt_newrelat, NULL);
  sqlite3_prepare_v2(db,
		     "SELECT nid1, fid1 FROM relat WHERE nid2=?, fid2=?",
		     -1, &stmt_getparent, NULL);
  sqlite3_prepare_v2(db,
		     "SELECT nid2, fid2 FROM relat WHERE nid1=?, fid1=?",
		     -1, &stmt_getchildren, NULL);
  sqlite3_prepare_v2(db,
		     "SELECT nid2, fid2 FROM relat "
		     "INNER JOIN files ON "
		     "relat.nid1=? AND relat.fid1=? AND "
		     "relat.nid2=files.nid AND relat.fid2=files.fid AND "
		     "files.name=?",
		     -1, &stmt_getnamedchild, NULL);

  //分区记录
  sqlite3_prepare_v2(db,
		     "INSERT INTO parts(nid, pid, dnid, ddid, fnid, ffid, uuid) "
		     "VALUES(?,?,?,?,?,?,?)",
		     -1, &stmt_newpart, NULL);
  sqlite3_prepare_v2(db,
		     "SELECT nid, pid FROM parts WHERE uuid=?",
		     -1, &stmt_findpart, NULL);

  //磁盘记录
  sqlite3_prepare_v2(db,
		     "INSERT INTO disks(nid, did, model, serno) "
		     "VALUES(?,?,?,?)",
		     -1, &stmt_newdisk, NULL);
  sqlite3_prepare_v2(db,
		     "SELECT nid, did FROM disks WHERE model=? AND serno=?",
		     -1, &stmt_finddisk, NULL);

  //属性记录
  sqlite3_prepare_v2(db,
		     "INSERT INTO props(nid, fid, key, value) "
		     "VALUES(?,?,?,?)",
		     -1, &stmt_newprop, NULL);
  sqlite3_prepare_v2(db,
		     "SELECT nid, fid FROM props WHERE key=? AND value=?",
		     -1, &stmt_findprop, NULL);

  sqlite3_stmt *stmt_tmp;
  sqlite3_prepare_v2(db,
		     "SELECT last_did, last_pid, last_fid FROM nodes WHERE nid=?",
		     -1, &stmt_tmp, NULL);
  sqlite3_bind_int(stmt_tmp, 1, local_nid);
  if(sqlite3_step(stmt_tmp) == SQLITE_ROW){
    local_last_did = sqlite3_column_int(stmt_tmp, 0);
    local_last_pid = sqlite3_column_int(stmt_tmp, 1);
    local_last_fid = sqlite3_column_int(stmt_tmp, 2);
  }
  sqlite3_finalize(stmt_tmp);
  npid_t p_tmp;
  ndid_t d_tmp;
  nfid_t f_tmp;
  sqlite3_prepare_v2(db,
		     "SELECT nid, pid, dnid, ddid, fnid, ffid FROM parts",
		     -1, &stmt_tmp, NULL);
  while(sqlite3_step(stmt_tmp) == SQLITE_ROW){
    p_tmp.nid = sqlite3_column_int(stmt_tmp, 0);
    p_tmp.pid = sqlite3_column_int(stmt_tmp, 1);
    d_tmp.nid = sqlite3_column_int(stmt_tmp, 2);
    d_tmp.did = sqlite3_column_int(stmt_tmp, 3);
    f_tmp.nid = sqlite3_column_int(stmt_tmp, 4);
    f_tmp.fid = sqlite3_column_int(stmt_tmp, 5);
    f2p[f_tmp] = p_tmp;
    p2f[p_tmp] = f_tmp;
    p2d[p_tmp] = d_tmp;
  }
  sqlite3_finalize(stmt_tmp);
  return 0;
}

int db_close()
{
  return sqlite3_close(db);
}

static int count_i(const nfid_t *p)
{
  if(!p){
    return 0;
  }
  int i=0;
  while(p->nid){
    p++;
    i++;
  }
  return i;
}

nfid_t db_newfile(const char* name, int64_t len, dbmode_t mode, bool fixed, bool sync,
		  int64_t atime, int64_t mtime, int64_t ctime)
{
  int st;

  st = sqlite3_exec(db, "BEGIN;", NULL, NULL, NULL);
  if(st != SQLITE_OK){
    fprintf(stderr, "begin faild, err=%d\n", st);
    goto err;
  }

  sqlite3_bind_int64(stmt_incfid, 1, local_last_fid+1);
  sqlite3_bind_int64(stmt_incfid, 2, local_nid);
  st = sqlite3_step(stmt_incfid);
  sqlite3_reset(stmt_incfid);
  if(st != SQLITE_DONE){
    sqlite3_exec(db, "ROLLBACK;", NULL, NULL, NULL);
    fprintf(stderr, "incfid step faild, err=%d\n", st);
    goto err;
  }
  sqlite3_bind_int64(stmt_newfile, 1, local_last_fid+1);
  sqlite3_bind_text(stmt_newfile, 2, name, strlen(name), NULL);
  sqlite3_bind_int64(stmt_newfile, 3, len);
  sqlite3_bind_int(stmt_newfile, 4, mode);
  sqlite3_bind_int(stmt_newfile, 5, fixed);
  sqlite3_bind_int(stmt_newfile, 6, sync);
  sqlite3_bind_int64(stmt_newfile, 7, atime);
  sqlite3_bind_int64(stmt_newfile, 8, mtime);
  sqlite3_bind_int64(stmt_newfile, 9, ctime);
  sqlite3_bind_int64(stmt_newfile, 10, time_ns64());
  st = sqlite3_step(stmt_newfile);
  sqlite3_reset(stmt_newfile);
  if(st != SQLITE_DONE){
    sqlite3_exec(db, "ROLLBACK;", NULL, NULL, NULL);
    fprintf(stderr, "newfile step faild, err=%d\n", st);
    goto err;
  }

  st = sqlite3_exec(db, "COMMIT;", NULL, NULL, NULL);
  if(st != SQLITE_OK){
    fprintf(stderr, "commit faild, err=%d\n", st);
    goto err;
  }
  local_last_fid++;
  return {local_nid, local_last_fid};
err:
  return {0, 0};
}

int db_newrelat(nfid_t parent, nfid_t child)
{
  int st;
  sqlite3_bind_int(stmt_newrelat, 1, parent.nid);
  sqlite3_bind_int(stmt_newrelat, 2, parent.fid);
  sqlite3_bind_int(stmt_newrelat, 3, child.nid);
  sqlite3_bind_int(stmt_newrelat, 4, child.fid);
  st = sqlite3_step(stmt_newrelat);
  sqlite3_reset(stmt_newrelat);
  return st==SQLITE_DONE?0:1;
}

//用于检测新文件要被储存到哪些分区
int db_getparts(set<npid_t> *p, nfid_t nfid)
{
  nfid_t tmp;
  deque<nfid_t> q;
  int count=0;
  q.push_back(nfid);
  while(q.size()){
    tmp = q[0];
    q.pop_front();
    sqlite3_bind_int(stmt_getparent, 1, tmp.nid);
    sqlite3_bind_int(stmt_getparent, 2, tmp.fid);
    while(sqlite3_step(stmt_getparent) == SQLITE_ROW){
      tmp.nid = sqlite3_column_int(stmt_getparent, 0);
      tmp.fid = sqlite3_column_int(stmt_getparent, 1);
      q.push_back(tmp);
      if(f2p.count(tmp)){
	p->insert(f2p[tmp]);
	count++;
      }
    }
  }
  return count;
}

//请不要直接使用，不会同步到ptab，请使用dp_newpart
npid_t db_newpart(ndid_t disk, const char *uuid)
{
  int st;
  npid_t part = {0, 0};
  int64_t ts_ns = time_ns64();
  nfid_t file = db_newfile(uuid, 0, DB_DIR, false, true, ts_ns, ts_ns, ts_ns);
  if(file.nid == 0){
    goto err;
  }
  sqlite3_bind_int(stmt_newpart, 1, local_nid);
  sqlite3_bind_int(stmt_newpart, 2, local_last_pid+1);
  sqlite3_bind_int(stmt_newpart, 3, disk.nid);
  sqlite3_bind_int(stmt_newpart, 4, disk.did);
  sqlite3_bind_int(stmt_newpart, 5, file.nid);
  sqlite3_bind_int(stmt_newpart, 6, file.fid);
  sqlite3_bind_text(stmt_newpart, 7, uuid, -1, NULL);
  st = sqlite3_step(stmt_newpart);
  sqlite3_reset(stmt_newpart);
  if(st != SQLITE_DONE){
    goto err;
  }
  local_last_pid++;
  sqlite3_bind_int(stmt_incpid, 1, local_nid);
  sqlite3_bind_int(stmt_incpid, 2, local_last_pid);
  sqlite3_step(stmt_incpid);
  part = {local_nid, local_last_pid};
  f2p[file] = part;
  p2f[part] = file;
  p2d[part] = disk;
err:
  return part;
}

npid_t db_findpart(const char *uuid)
{
  npid_t ret = {0, 0};
  sqlite3_bind_text(stmt_findpart, 1, uuid, -1, NULL);
  while(sqlite3_step(stmt_findpart) == SQLITE_ROW){
    ret.nid = sqlite3_column_int(stmt_findpart, 0);
    ret.pid = sqlite3_column_int(stmt_findpart, 1);
  }
  sqlite3_reset(stmt_findpart);
  return ret;
}

ndid_t db_newdisk(const char *model, const char* serno)
{
  int stat;
  sqlite3_bind_int(stmt_newdisk, 1, local_nid);
  sqlite3_bind_int(stmt_newdisk, 2, local_last_did+1);
  sqlite3_bind_text(stmt_newdisk, 3, model, -1, NULL);
  sqlite3_bind_text(stmt_newdisk, 4, serno, -1, NULL);
  stat = sqlite3_step(stmt_newdisk);
  sqlite3_reset(stmt_newdisk);
  if(stat != SQLITE_DONE){
    return {0, 0};
  }
  local_last_did++;
  sqlite3_bind_int(stmt_incdid, 1, local_last_did);
  sqlite3_bind_int(stmt_incdid, 2, local_nid);
  sqlite3_step(stmt_incdid);
  sqlite3_reset(stmt_incdid);
  return {local_nid, local_last_did};
}

ndid_t db_finddisk(const char *model, const char* serno)
{
  ndid_t ret={0, 0};
  sqlite3_bind_text(stmt_finddisk, 1, model, -1, NULL);
  sqlite3_bind_text(stmt_finddisk, 2, serno, -1, NULL);
  while(sqlite3_step(stmt_finddisk) == SQLITE_ROW){
    ret.nid = sqlite3_column_int(stmt_finddisk, 0);
    ret.did = sqlite3_column_int(stmt_finddisk, 1);
  }
  sqlite3_reset(stmt_finddisk);
  return ret;
}

nfid_t db_getnamedchild(nfid_t base, const char *name)
{
  nfid_t ret = {0,0};
  sqlite3_bind_int(stmt_getnamedchild, 1, base.nid);
  sqlite3_bind_int(stmt_getnamedchild, 2, base.fid);
  sqlite3_bind_text(stmt_getnamedchild, 3, name, -1, NULL);
  if(sqlite3_step(stmt_getnamedchild) == SQLITE_ROW){
    ret.nid = sqlite3_column_int(stmt_getnamedchild, 0);
    ret.fid = sqlite3_column_int(stmt_getnamedchild, 1);
  }
  sqlite3_reset(stmt_getnamedchild);
  return ret;
}

int db_newprop(nfid_t nfid, const char *key, const void *data, size_t length)
{
  int stat;
  sqlite3_bind_int(stmt_newprop, 1, nfid.nid);
  sqlite3_bind_int(stmt_newprop, 2, nfid.fid);
  sqlite3_bind_text(stmt_newprop, 3, key, -1, NULL);
  sqlite3_bind_blob(stmt_newprop, 4, data, length, NULL);
  stat = sqlite3_step(stmt_newprop);
  sqlite3_reset(stmt_newprop);
  if(stat != SQLITE_DONE){
    return 1;
  }else{
    return 0;
  }
}

nfid_t db_findprop(const char *key, const void *data, size_t length)
{
  nfid_t res;
  sqlite3_bind_text(stmt_findprop, 1, key, -1, NULL);
  sqlite3_bind_blob(stmt_findprop, 2, data, length, NULL);
  if(sqlite3_step(stmt_findprop) == SQLITE_ROW){
    res.nid = sqlite3_column_int(stmt_findprop, 0);
    res.fid = sqlite3_column_int(stmt_findprop, 1);
  }else{
    res.nid = 0;
    res.fid = 0;
  }
  sqlite3_reset(stmt_findprop);
  return res;
}

int inotifypath_init(inotifypath_stat *&p)
{
  p = new inotifypath_stat();
  sqlite3_prepare_v2(db,
		     "SELECT nid,fid,name FROM files WHERE sync=1",
		     -1, &p->stmt_sync1, NULL);
  sqlite3_prepare_v2(db,
		     "SELECT nid,fid,name FROM files INNER JOIN relat ON "
		     "relat.nid1=files.nid AND relat.fid1=files.fid AND relat.nid2=? AND relat.fid2=?",
		     -1, &p->stmt_parent, NULL);
  p->find_parent_finish = true;
}

int inotifypath_step(inotifypath_stat *p, string &pout)
{
  sino stmp;
  while(1){
    if(p->q.size() == 0){
      if(sqlite3_step(p->stmt_sync1) == SQLITE_ROW){
	stmp.file.nid = sqlite3_column_int(p->stmt_sync1, 0);
	stmp.file.fid = sqlite3_column_int(p->stmt_sync1, 1);
	stmp.path = (const char*)sqlite3_column_text(p->stmt_sync1, 2);
	p->q.push_back(stmp);
      }else{
	sqlite3_finalize(p->stmt_parent);
	sqlite3_finalize(p->stmt_sync1);
	delete p;
	return 1; //完了
      }
    }
    while(p->q.size()){
      if(p->find_parent_finish){
	sqlite3_bind_int(p->stmt_parent, 1, p->q[0].file.nid);
	sqlite3_bind_int(p->stmt_parent, 2, p->q[0].file.fid);
	p->find_parent_finish = false;
      }
      while(sqlite3_step(p->stmt_parent) == SQLITE_ROW){
	stmp.file.nid = sqlite3_column_int(p->stmt_parent, 0);
	stmp.file.fid = sqlite3_column_int(p->stmt_parent, 1);
	stmp.path = string((const char*)sqlite3_column_text(p->stmt_parent, 2)) + '/' + p->q[0].path;
	p->q.push_back(stmp);
	if(f2p.count(stmp.file)){
	  partinfo *pinfo = ptab.get_bynpid(f2p[stmp.file]);
	  if(pinfo){
	    pout = pinfo->moupath + '/' + p->q[0].path;
	    return 0; //找到一个
	  }
        }
      }
      sqlite3_reset(p->stmt_parent);
      p->q.pop_front();
      p->find_parent_finish = true;
    }
  }
}
