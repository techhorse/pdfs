/* pdfsd.cc: daemon of pdfs
 * Copyright (C) 2023  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "misc.h"
#include "readconf.h"
#include "db.hh"
#include "diskpart.hh"
#include "msgproc.hh"
#include "log.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <string>
#include <iostream>
#include <thread>

using namespace std;

int inothread_fn()
{
  //读取inotify列表
  inotifypath_stat *ips;
  string s;
  inotifypath_init(ips);
  while(inotifypath_step(ips, s) == 0){
    cout << s << endl;
  }
  //监视inotify
}

struct{
  MsgCMD name;
  MsgRsp (*func)(const void *pmsg, size_t length, void *pout, size_t &osize);
}const msgcmds[] = {
  {MsgCMD_AddFile, msgcmd_addfile},
  {MsgCMD_FindProp, msgcmd_findprop},
};

int msgthread_fn()
{
  ssize_t size;
  struct msgcmd msg;
  key_t ki = ftok(PATH_IPC, 'i');
  if(ki == -1){
    LOGE_ERRNO("ki");
    return -1;
  }
  key_t ko = ftok(PATH_IPC, 'o');
  if(ko == -1){
    LOGE_ERRNO("ko");
    return -1;
  }
  //TODO: 改为socket, 避免与多个程序同时通信产生混乱
  int ki_qid = msgget(ki, IPC_CREAT | 0660);
  int ko_qid = msgget(ko, IPC_CREAT | 0660);
  while(1){
    size = msgrcv(ki_qid, &msg, sizeof(msg)-sizeof(long), 0, 0);
    for(int i=0;i<ARRLEN(msgcmds);i++){
      if(msgcmds[i].name == msg.mtype){
	size_t tsize = 0;
	MsgRsp rsp = msgcmds[i].func(msg.mtext, size, msg.mtext, tsize);
	if(rsp == MsgRsp_OK && tsize > 0){
	  msgsnd(ko_qid, &msg, sizeof(pid_t)+tsize, 0);
	}
	break;
      }
    }
    cout << msg.mtype << ' ' << msg.pid << ' ' << msg.mtext << endl;
  }
}

int main(int argc, char *argv[])
{
  sqlite3_config(SQLITE_CONFIG_MULTITHREAD);
  load_conf(PATH_CONF);
  db_init(PATH_DB);
  dp_init();
  thread th_ino(inothread_fn);
  thread th_msg(msgthread_fn);
  th_ino.join();
  th_msg.join();
}
