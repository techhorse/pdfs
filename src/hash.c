/* hash.c
 * Copyright (C) 2024  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "hash.h"
#include <stdlib.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

typedef int (*HASH_Init_t)(void *);
typedef int (*HASH_Update_t)(void *, const void *, size_t);
typedef int (*HASH_Final_t)(unsigned char *, void *);

typedef struct{
  HASH_Init_t init;
  HASH_Update_t update;
  HASH_Final_t final;
  size_t ctx_length;
  size_t digest_length;
}Hash_Handler_t;

const Hash_Handler_t hash_handler[] = {
  {(HASH_Init_t)MD5_Init, (HASH_Update_t)MD5_Update, (HASH_Final_t)MD5_Final, sizeof(MD5_CTX), MD5_DIGEST_LENGTH},
  {(HASH_Init_t)SHA1_Init, (HASH_Update_t)SHA1_Update, (HASH_Final_t)SHA1_Final, sizeof(SHA_CTX), SHA_DIGEST_LENGTH},
  {(HASH_Init_t)SHA256_Init, (HASH_Update_t)SHA256_Update, (HASH_Final_t)SHA256_Final, sizeof(SHA256_CTX), SHA256_DIGEST_LENGTH},
  {(HASH_Init_t)SHA512_Init, (HASH_Update_t)SHA512_Update, (HASH_Final_t)SHA512_Final, sizeof(SHA512_CTX), SHA512_DIGEST_LENGTH},
};

size_t calc_hashs_length(unsigned long hflags)
{
  size_t s = 0;
  for(int i=0;i<NHASH_SUPPORT;i++){
    if(hflags & (1UL<<i)){
      s += hash_handler[i].digest_length;
    }
  }
  return s;
}

int calc_file_hashs(const char *path, unsigned long hflags, uint8_t *out)
{
  if(hflags == 0){
    return 0;
  }
  FILE *fp = fopen(path, "r");
  if(fp == NULL){
    return -1;
  }

  uint8_t *buffer = malloc(READ_BLOCKSIZE);
  if(buffer == NULL){
    fclose(fp);
    return -1;
  }
  int nhash = __builtin_popcount(hflags);
  void **ctxs = malloc(nhash*sizeof(void *));
  if(ctxs == NULL){
    fclose(fp);
    free(buffer);
    return -1;
  }
  void **ctx_;

  ctx_ = ctxs;
  for(int i=0;i<NHASH_SUPPORT;i++){
    if(hflags & (1UL<<i)){
      *ctx_ = malloc(hash_handler[i].ctx_length);
      if(*ctx_ == NULL){
	fclose(fp);
	free(buffer);
	for(void **p=ctxs;p<ctx_;p++){
	  free(*p);
	}
	return -1;
      }
      hash_handler[i].init(*ctx_);
      ctx_++;
    }
  }

  while(1){
    size_t size_read = fread(buffer, 1, READ_BLOCKSIZE, fp);
    ctx_ = ctxs;
    for(int i=0;i<NHASH_SUPPORT;i++){
      if(hflags & (1UL<<i)){
	hash_handler[i].update(*ctx_++, buffer, size_read);
      }
    }
    if(size_read != READ_BLOCKSIZE){
      break;
    }
  }
  free(buffer);
  fclose(fp);

  ctx_ = ctxs;
  for(int i=0;i<NHASH_SUPPORT;i++){
    if(hflags & (1UL<<i)){
      hash_handler[i].final(out, *ctx_);
      free(*ctx_);
      ctx_++;
      out += hash_handler[i].digest_length;
    }
  }
  free(ctxs);
  return 0;
}
