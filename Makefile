# Makefile: makefile of pdfs
# Copyright (C) 2023  Xu Ruijun
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

INCS += -Iinclude

OBJF += build/db.o
OBJF += build/readconf.o
OBJF += build/diskpart.o
OBJF += build/msgproc.o
OBJF += build/misc.o
OBJF += build/hash.o

CC=gcc
CXX=g++
LINK=g++
CFLAGS=$(INCS) -g
CXXFLAGS=$(INCS) -g
LDFLAGS=-lsqlite3 -lblkid -lpthread -lcrypto -lssl -lbsd

build/%.o: src/%.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $^ -c -o $@

build/%.o: src/%.cc
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $^ -c -o $@

all: cli pdfsd

cli: $(OBJF) build/cli.o
	$(LINK) $^ $(LDFLAGS) -o $@

pdfsd: $(OBJF) build/pdfsd.o
	$(LINK) $^ $(LDFLAGS) -o $@

clean:
	rm -rf build
	rm -f cli
	rm -f pdfsd
