# PDFS个人分布式文件系统
PDFS(Personal Distributed File System)是一个专为个人储存文件而设计的分布式文件系统。

## 背景
为了防止文件丢失，我通常手动复制文件备份；为了储存大量不经常使用的数据，通常会把硬盘卸下或刻录到光盘。
但是没有一个统一的管理工具会变得很麻烦很糟糕，需要逐个手动复制文件，有时候还会难以找到所需要的文件。
网上看到NAS很流行，但是我认为过于中心化，遇到故障无法使用，手动预先下载很麻烦。
我想要实现一个分布式文件系统，安装在多台电脑上，实现尽可能多的功能，还要较强的安全性。

## 功能目标
- 可以使用U盘、光盘等需要手动加载的储存介质
- 为了省电而通常关机的节点使用WOL(Wake On Lan)唤醒
- 自动同步文件和文件夹
- 挂载PDFS虚拟文件系统
- 强大可扩展性，与相关应用程序协调
- 无法直接连接的两个节点可通过移动储存介质同步数据
- 按网络的带宽、延时、计费方式自动选择合适的同步内容
- 容忍分区，分区和合并无缝切换

## 相关资料
[设计文档](design.md)
