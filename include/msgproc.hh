/* msgproc.hh: process of message queue
 * Copyright (C) 2023-2024  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MSGPROC_HH
#define MSGPROC_HH

#include "misc.h"

typedef struct{
  bool fixed;
  bool sync;
  bool recursive;
  bool entry_compress;
  bool follow;
  int64_t bigfile_thresold;
}Para_cmd_addfile;

typedef struct{
  uint8_t len_name; //含'\0'的长度
  uint8_t len_data;
}Para_cmd_findprop;

MsgRsp msgcmd_addfile(const void *pmsg, size_t length, void *pout, size_t &osize);
MsgRsp msgcmd_findprop(const void *pmsg, size_t length, void *pout, size_t &osize);

#endif
