/* diskpart.cc: Get disks and parts info
 * Copyright (C) 2023  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DISKPART_HH
#define DISKPART_HH

#include "db.hh"

#ifdef __cplusplus
#include <vector>
#include <string>

using namespace std;

inline bool operator==(npid_t x, npid_t y)
{
  return x.nid == y.nid and x.pid == y.pid;
}

typedef struct{
  npid_t npid;
  string uuid;
  string devpath;
  string moupath;
}partinfo;

class PartsTab{
private:
  vector<partinfo> vec;
public:
  int add(partinfo &p);
  partinfo *get_bynpid(const npid_t &npid);
  partinfo *get_byuuid(const string &uuid);
  partinfo *get_bydevpath(const string &devpath);
  partinfo *get_bymoupath(const string &mpath);
  void print();
};
#endif

#ifdef __cplusplus
extern "C"{
#endif
int disk_getinfo(const char *devpath, char *model, char *serno);
int path_abs_nolink(const char *path, char *buf, size_t bufsize);
int path2mpcut(const char *path);
int part_getuuid(const char *path, char *buf, size_t bufsize);
int dp_newpart(ndid_t disk, partinfo *p);
int dp_init();
#ifdef __cplusplus
}
#endif

#endif
