/* misc.h
 * Copyright (C) 2023  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MISC_H
#define MISC_H

#include <stdint.h>

#include <sys/types.h>

#define PATH_CONF "pdfs.conf"
#define PATH_DB   "pdfs.db"
#define PATH_IPC  "ipc"
#define ARRLEN(x) (sizeof((x))/sizeof((x)[0]))

typedef enum{
  MsgCMD_AddFile = 1,
  MsgCMD_FindProp = 2,
}MsgCMD;

typedef enum{
  MsgRsp_OK = 0,
  MsgRsp_NodeNF,
  MsgRsp_DiskNF,
  MsgRsp_PartNF,
  MsgRsp_FileNF,
  MsgRsp_DBErr,
  MsgRsp_GetInfoErr,
  MsgRsp_OtherErr,
  MsgRsp_ParaErr,
}MsgRsp;

struct msgcmd{
  long mtype;
  pid_t pid;
  char mtext[1024];
};

#ifdef __cplusplus
extern "C"{
#endif

int64_t time_ns64();
int64_t ts_ns64(struct timespec ts);
size_t hexstr_to_u8arr(const char* str, uint8_t *data, size_t outsize);

#ifdef __cplusplus
}
#endif

#endif
