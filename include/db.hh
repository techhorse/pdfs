/* db.h: Packed database API
 * Copyright (C) 2023  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DB_HH
#define DB_HH

#include <stdint.h>
#include <stdbool.h>

#include "sqlite3.h"

#include <string>
#include <deque>

using namespace std;

typedef struct{
  int64_t nid;
  int64_t did;
}ndid_t;

typedef struct{
  int64_t nid;
  int64_t pid;
}npid_t;

typedef struct{
  int64_t nid;
  int64_t fid;
}nfid_t;

typedef enum{
  DB_FILE = 0,
  DB_DIR,
  DB_LNK,
}dbmode_t;

typedef struct{
  nfid_t file;
  string path;
}sino;

typedef struct{
  deque<sino> q;
  sqlite3_stmt *stmt_sync1;
  sqlite3_stmt *stmt_parent;
  bool find_parent_finish;
}inotifypath_stat;

#ifdef __cplusplus
inline bool operator<(const ndid_t x, const ndid_t y)
{
  return x.nid < y.nid or (x.nid == y.nid and x.did < y.did);
}

inline bool operator<(const npid_t x, const npid_t y)
{
  return x.nid < y.nid or (x.nid == y.nid and x.pid < y.pid);
}

inline bool operator<(const nfid_t x, const nfid_t y)
{
  return x.nid < y.nid or(x.nid == y.nid and x.fid < y.fid);
}

inline bool operator<(const sino x, const sino y)
{
  return x.file < y.file;
}

int inotifypath_init(inotifypath_stat *&p);
int inotifypath_step(inotifypath_stat *p, string &pout);
#endif

#ifdef __cplusplus
extern "C"{
#endif

int db_init(const char *fn);
int db_close();
nfid_t db_newfile(const char* name, int64_t len, dbmode_t mode, bool fixed, bool sync,
		  int64_t atime, int64_t mtime, int64_t ctime);
int db_newrelat(nfid_t parent, nfid_t child);
npid_t db_newpart(ndid_t disk, const char *uuid);
npid_t db_findpart(const char *uuid);
ndid_t db_newdisk(const char *model, const char* serno);
ndid_t db_finddisk(const char *model, const char* serno);
nfid_t db_getnamedchild(nfid_t base, const char* name);
int db_newprop(nfid_t nfid, const char *key, const void *data, size_t length);
nfid_t db_findprop(const char *key, const void *data, size_t length);
nfid_t db_mkdirs(nfid_t base, const char *path);

#ifdef __cplusplus
}
#endif

#endif
