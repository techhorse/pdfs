/* hash.h
 * Copyright (C) 2024  Xu Ruijun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef HASH_H
#define HASH_H

#include <stdio.h>
#include <stdint.h>

#define FLAG_MD5    (1UL<<0)
#define FLAG_SHA1   (1UL<<1)
#define FLAG_SHA256 (1UL<<2)
#define FLAG_SHA512 (1UL<<3)
#define FLAG_HASHALL (FLAG_MD5|FLAG_SHA1|FLAG_SHA256|FLAG_SHA512)

#define NHASH_SUPPORT 4
#define READ_BLOCKSIZE    1048576

#ifdef __cplusplus
extern "C"{
#endif

size_t calc_hashs_length(unsigned long hflags);
int calc_file_hashs(const char *path, unsigned long hflags, uint8_t *out);

#ifdef __cplusplus
}
#endif

#endif
